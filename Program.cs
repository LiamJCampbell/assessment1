﻿using System;

namespace assessment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Greetings valued customer, welcome To Liam's Emporium");
            Console.WriteLine("What is your name? ");
            String Custname = Console.ReadLine();
            Console.WriteLine($"Hello {Custname}!, please enter a number with two decimal places ");
            double number1 = double.Parse(Console.ReadLine());
            Console.WriteLine($"You entered {number1}, would you like to add another item? ");
            String Yes = Console.ReadLine();
            if (Yes == "yes")
            {
                Console.WriteLine("Please enter your 2nd number");
                number1 += double.Parse(Console.ReadLine());
                Console.WriteLine($"You total before GST is ${number1}");
            }
            else
            {
            Console.WriteLine($"Your total before GST is {number1}");    
            }
        {
            double number2 = number1 * 1.15;
            Console.WriteLine($"Your total including GST is ${number2}");
            Console.WriteLine("Thank you for shopping at Liam's Emporium!");
        }
        }
    }
}
